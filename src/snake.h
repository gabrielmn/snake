#ifndef SNAKE_H
#define SNAKE_H

#include "direction.h"
#include "position.h"

class Snake
{
private:
	Position position;
	bool is_body;
	Snake *next_body;

public:
	static const unsigned char HEAD_SYMBOL = 'O';
	static const unsigned char BODY_SYMBOL = '#';

	/**
	 * @brief Construct a new Snake object
	 *
	 */
	Snake();

	/**
	 * @brief Construct a new Snake object
	 *
	 * @param position
	 * @param is_body
	 */
	Snake(const Position &position, const bool is_body);

	/**
	 * @brief Destroy the Snake object
	 *
	 */
	~Snake();

	Snake *get_next() const;

	/**
	 * @brief Get the position object
	 *
	 * @return Position
	 */
	Position get_position() const;

	unsigned char get_symbol() const;

	/**
	 * @brief Check if there is another part of the snake after this part.
	 *
	 * @return true
	 * @return false
	 */
	bool has_next() const;

	void increase_snake(const Direction direction);

	/**
	 * @brief Set the position of th
	 *
	 * @param position
	 */
	void set_position(const Position &position);

	void set_next(Snake *snake);
};

#endif