#ifndef COLLISION_EXCEPTION_H
#define COLLISION_EXCEPTION_H

#include <exception>
#include <string>

class CollisionException:public std::exception
{
public:
	CollisionException(std::string msg);
	std::string getMessage();
	void addMessage(std::string msg);
	~CollisionException();
private:
	std::string msg;
};

#endif