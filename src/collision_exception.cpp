#include "collision_exception.h"


CollisionException::CollisionException(std::string msg)
{
	this->msg = msg;
}


std::string CollisionException::getMessage()
{
	return this->msg;
}

void CollisionException::addMessage(std::string msg)
{
	this->msg += msg;
}

CollisionException::~CollisionException()
{
}
