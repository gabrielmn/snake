#include "screen.h"
#ifdef __unix__
#include <ncurses.h>
#elif __WIN32
// #include <conio.h>
#endif

Screen::Screen()
{
#ifdef __unix__
    // start ncurses
    initscr();
    // hides the cursor
    curs_set(0);
#elif __WIN32
// #include <conio.h>
#endif
}

Screen::~Screen()
{
#ifdef __unix__
    // end ncurses
    endwin();
#elif __WIN32
// #include <conio.h>
#endif
}

void Screen::draw_pixel(const Position &position, const char pixel) const
{
#ifdef __unix__

    mvaddch(position.y, position.x, pixel);
    refresh();

#elif __WIN32
// #include <conio.h>
#endif
}

void Screen::draw_pixel(const Position &position, const std::string pixels) const
{
#ifdef __unix__

    mvaddstr(position.y, position.x, pixels.c_str());
    refresh();

#elif __WIN32
// #include <conio.h>
#endif
}

void Screen::clear()
{
    throw "Not implemented";
}