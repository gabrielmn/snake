#ifndef APPLE_H
#define APPLE_H

#include "position.h"

class Apple
{
private:
	
	Position position;

public:
	static const unsigned char SYMBOL = '+';

	Apple(){};
	Apple(const Position& position): position(position){};

	void set_position(const Position &position);
	Position get_position() const;
};

#endif