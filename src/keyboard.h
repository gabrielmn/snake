#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "key.h"

class Keyboard
{

public:
    Keyboard();

    bool key_pressed() const;

    Key get_key_pressed() const;
};

#endif