#include "position.h"
#include <cmath>

Position Position::floor()
{
    return Position(std::floor(this->x), std::floor(this->y));
}

bool operator==(const Position &lhs, const Position &rhs)
{
    return (lhs.x == rhs.x) && (lhs.y == rhs.y);
}

bool operator!=(const Position &lhs, const Position &rhs)
{
    return !(lhs == rhs);
}