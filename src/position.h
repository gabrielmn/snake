#ifndef POSITION_H
#define POSITION_H

class Position
{
public:
    float x;
    float y;

    Position(): x(0), y(0){};
    Position(const float &x, const float &y): x(x), y(y){};

    Position floor();

    friend bool operator==(const Position& lhs, const Position& rhs);
    friend bool operator!=(const Position& lhs, const Position& rhs);
    
};

#endif