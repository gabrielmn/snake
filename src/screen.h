#ifndef SCREEN_H
#define SCREEN_H

#include "position.h"
#include <string>

class Screen
{

public:

    Screen();
    ~Screen();

    void draw_pixel(const Position &position, const char pixel) const;

    void draw_pixel(const Position &position, const std::string pixels) const;

    void clear();

};

#endif