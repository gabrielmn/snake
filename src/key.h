#ifndef KEY_H
#define KEY_H

#include <iostream>
#include <string>

enum class Key
{
    // arrows
    ARROW_UP,
    ARROW_DOWN,
    ARROW_LEFT,
    ARROW_RIGHT,
    // lowercase
    LOWERCASE_W,
    LOWERCASE_S,
    LOWERCASE_A,
    LOWERCASE_D,
    // uppercase
    UPPERCASE_W,
    UPPERCASE_S,
    UPPERCASE_A,
    UPPERCASE_D,
};

#endif