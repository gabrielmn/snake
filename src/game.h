#ifndef GAME_H
#define GAME_H

#include "screen.h"
#include "keyboard.h"
#include "direction.h"

#include "snake.h"
#include "apple.h"

#include <chrono>

class Game
{

	static const unsigned short SCOREBOARD_HEIGHT = 5;

	static const unsigned short HEIGHT = 30; // 24
	static const unsigned short WIDTH = 50; // 48

	static constexpr float VERTICAL_SPEED = 0.24; 
	static constexpr float HORIZONTAL_SPEED = 0.48; 

	static const bool DEBUG = true;

	Screen screen;
	Keyboard keyboard;

	Direction direction;

	Snake snake;
	Apple apple;

	unsigned int score = 0;

	void initialize();

	Apple generate_apple() const;

	void draw_snake() const;

	void draw_apple() const;

	void erase_snake() const;

	void erase_apple() const;

	void draw_scoreboard();

	void update_scoreboard();

	void controller();

	void move_snake();

	void debug(const double move_snake_time, const double draw_snake_time, const double draw_apple_time, const double update_scoreboard_time);

public:
	
	Game();
	~Game();

	void run();
	
};

#endif
