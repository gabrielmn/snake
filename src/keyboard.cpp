#include "keyboard.h"

#ifdef __unix__
#include <ncurses.h>
#elif __WIN32
#include <conio.h>
#endif

Keyboard::Keyboard()
{
#ifdef __unix__
    // enable special keys
    keypad(stdscr, TRUE);
    // disable keypress feedback
    noecho();
    // disable terminal buffer, making possible to read the key as is pressed
    cbreak();
    // disable waiting for user
    nodelay(stdscr, TRUE);
    // raw();
#elif __WIN32

#endif
}

bool Keyboard::key_pressed() const
{
#ifdef __unix__

    int key = wgetch(stdscr);
    if (key == ERR)
        return false;

    ungetch(key);
    return true;
#elif __WIN32
    return kbhit();
#endif
}

Key Keyboard::get_key_pressed() const
{
    int key = wgetch(stdscr);
    if (key == ERR)
        throw "ERR";

    switch (key)
    {
    case KEY_UP:
        return Key::ARROW_UP;
    case KEY_DOWN:
        return Key::ARROW_DOWN;
    case KEY_LEFT:
        return Key::ARROW_LEFT;
    case KEY_RIGHT:
        return Key::ARROW_RIGHT;
    case 'w':
        return Key::LOWERCASE_W;
    case 's':
        return Key::LOWERCASE_S;
    case 'a':
        return Key::LOWERCASE_A;
    case 'd':
        return Key::LOWERCASE_D;
    case 'W':
        return Key::UPPERCASE_W;
    case 'S':
        return Key::UPPERCASE_S;
    case 'A':
        return Key::UPPERCASE_A;
    case 'D':
        return Key::UPPERCASE_D;
    default:
        throw "Key not implemented";
    }
}
