#include "snake.h"

Snake::Snake() : next_body(nullptr)
{

};

Snake::Snake(const Position &position, const bool is_body) : position(position), is_body(is_body), next_body(nullptr)
{

}

Snake::~Snake()
{
	if (this->next_body != nullptr)
		delete this->next_body;
}

Snake *Snake::get_next() const
{
	return this->next_body;
}

Position Snake::get_position() const
{
	return this->position;
}

unsigned char Snake::get_symbol() const
{
	if (this->is_body)
		return Snake::BODY_SYMBOL;

	return Snake::HEAD_SYMBOL;
}

bool Snake::has_next() const
{
	return (this->next_body != nullptr);
}

void Snake::increase_snake(const Direction direction)
{
	Snake *snake = this;
	while (snake->has_next())
		snake = snake->get_next();

	Position position = snake->get_position();
	switch (direction)
	{
	case Direction::UP:
		--position.y;
		break;
	case Direction::DOWN:
		++position.y;
		break;
	case Direction::RIGHT:
		++position.x;
		break;
	case Direction::LEFT:
		--position.x;
		break;
	}
	snake->set_next(new Snake(position, true));
}

void Snake::set_next(Snake *snake)
{
	this->next_body = snake;
}

void Snake::set_position(const Position &position)
{
	this->position = position;
}