#include "game.h"

#include "key.h"
#include "collision_exception.h"


#include <random>
#include <chrono>
#include <thread>
#include <cmath>

#include <string>

Game::Game()
{
	this->screen = Screen();
	this->keyboard = Keyboard();
	this->direction = Direction::RIGHT;

	this->initialize();
}

void Game::initialize()
{
	// draw scoreboard
	this->draw_scoreboard();
	// draw map
	for (unsigned short y = Game::SCOREBOARD_HEIGHT; y < Game::HEIGHT; y++)
	{
		for (unsigned short x = 0; x < Game::WIDTH; x++)
		{
			if (y == 5 || y == (Game::HEIGHT - 1))
			{
				this->screen.draw_pixel(Position(x, y), '=');
			}
			else if (x == 0 || x == (Game::WIDTH - 1))
			{
				this->screen.draw_pixel(Position(x, y), '|');
			}
		}
	}

	// draw snake
	this->snake = Snake(Position(Game::WIDTH / 2, Game::HEIGHT / 2), false);
	this->draw_snake();

	// generate apple
	this->apple = this->generate_apple();
	// draw apple
	this->draw_apple();
}

Apple Game::generate_apple() const
{
	std::random_device random;
	unsigned short y = random() % (Game::HEIGHT - Game::SCOREBOARD_HEIGHT - 3) + Game::SCOREBOARD_HEIGHT + 1;
	unsigned short x = random() % (Game::WIDTH - 2) + 1;
	return Apple(Position(x, y));
}

void Game::draw_snake() const
{
	const Snake *snake = &this->snake;
	while (snake->has_next())
	{
		this->screen.draw_pixel(snake->get_position(), snake->get_symbol());
		snake = snake->get_next();
	}
	this->screen.draw_pixel(snake->get_position(), snake->get_symbol());
}

void Game::draw_apple() const
{
	this->screen.draw_pixel(this->apple.get_position(), Apple::SYMBOL);
}

void Game::erase_snake() const
{

	const Snake *snake = &this->snake;
	while (snake->has_next())
	{
		this->screen.draw_pixel(snake->get_position(), ' ');
		snake = snake->get_next();
	}
	this->screen.draw_pixel(snake->get_position(), ' ');
}

void Game::erase_apple() const
{
	this->screen.draw_pixel(this->apple.get_position(), ' ');
}

void Game::draw_scoreboard()
{
	for (unsigned short y = 0; y < Game::SCOREBOARD_HEIGHT; y++)
	{
		for (unsigned short x = 0; x < Game::WIDTH; x++)
		{
			if (y == 0 || y == 4)
			{
				this->screen.draw_pixel(Position(x, y), '=');
			}
			else if (x == 0 || x == (Game::WIDTH - 1))
			{
				this->screen.draw_pixel(Position(x, y), '|');
			}
		}
	}
	std::string str("scoreboard: ");
	str.append(std::to_string(this->score));
	this->screen.draw_pixel(Position(3, std::floor(Game::SCOREBOARD_HEIGHT / 2)), str);
}

void Game::update_scoreboard(){
	
	std::string str("scoreboard: ");
	str.append(std::to_string(this->score));
	this->screen.draw_pixel(Position(3, std::floor(Game::SCOREBOARD_HEIGHT / 2)), str);

}

void Game::controller()
{
	if (this->keyboard.key_pressed())
	{
		Key key = keyboard.get_key_pressed();
		if (key == Key::ARROW_UP || key == Key::LOWERCASE_W || key == Key::UPPERCASE_W)
		{
			this->direction = Direction::UP;
		}
		else if (key == Key::ARROW_DOWN || key == Key::LOWERCASE_S || key == Key::UPPERCASE_S)
		{
			this->direction = Direction::DOWN;
		}
		else if (key == Key::ARROW_LEFT || key == Key::LOWERCASE_A || key == Key::UPPERCASE_A)
		{
			this->direction = Direction::LEFT;
		}
		else if (key == Key::ARROW_RIGHT || key == Key::LOWERCASE_D || key == Key::UPPERCASE_D)
		{
			this->direction = Direction::RIGHT;
		}
	}
}

void Game::move_snake()
{
	// move head and preserve head old position
	Position position = this->snake.get_position();
	Position old_position = this->snake.get_position().floor();
	switch (this->direction)
	{
	case Direction::UP:
		position.y -= Game::VERTICAL_SPEED;
		if(std::floor(position.y) == Game::SCOREBOARD_HEIGHT)
			throw CollisionException("hit upper wall");
		snake.set_position(position);

		break;
	case Direction::DOWN:
		position.y += Game::VERTICAL_SPEED;
		if(std::floor(position.y) == Game::HEIGHT - 1)
			throw CollisionException("hit upper wall");
		snake.set_position(position);
		
		break;
	case Direction::RIGHT:
		position.x += Game::HORIZONTAL_SPEED;
		if(std::floor(position.x) == Game::WIDTH - 1)
			throw CollisionException("hit upper wall");
		snake.set_position(position);
		
		break;
	case Direction::LEFT:
		position.x -= Game::HORIZONTAL_SPEED;
		if(std::floor(position.x) == 0)
			throw CollisionException("hit upper wall");
		snake.set_position(position);
		
		break;
	}

	// eat apple
	if(position.floor() == this->apple.get_position()){
		this->snake.increase_snake(direction);
		this->score++;
		this->erase_apple();
		this->apple = this->generate_apple();
	}

	// erase snake head
	this->screen.draw_pixel(old_position, " "); // move to draw snake
	// move body
	if (this->snake.has_next())
	{	
		Snake *snake = this->snake.get_next();
		while (snake->has_next())
		{	
			Position temp = snake->get_position(); // hold old position
			snake->set_position(old_position);
			old_position = temp; // set next body position
			snake = snake->get_next();
		}
		// erase snake last tail piece
		this->screen.draw_pixel(snake->get_position(), " "); // move to draw snake
		snake->set_position(old_position);
	}

	// check collision with body
	if (this->snake.has_next())
	{
		Snake *snake = this->snake.get_next();
		while (snake->has_next())
		{	
			if(this->snake.get_position() == snake->get_position())
				throw CollisionException("hit body");
			
			snake = snake->get_next();
		}
		if(this->snake.get_position() == snake->get_position())
			throw CollisionException("hit body");
			
	}
}

void Game::debug(const double move_snake_time, const double draw_snake_time, const double draw_apple_time, const double update_scoreboard_time)
{
	this->screen.draw_pixel(Position(Game::WIDTH, 0), "0");
	this->screen.draw_pixel(Position(Game::WIDTH, 5), "5");
	this->screen.draw_pixel(Position(Game::WIDTH, Game::HEIGHT -1), std::to_string(Game::HEIGHT -1));
	
	this->screen.draw_pixel(Position(0, Game::HEIGHT), "0");
	this->screen.draw_pixel(Position(Game::WIDTH - 1, Game::HEIGHT), std::to_string(Game::WIDTH -1));
	
	std::string snake_pos("(" + std::to_string(this->snake.get_position().x) + "," + std::to_string(this->snake.get_position().y) + ")");
	this->screen.draw_pixel(Position(Game::WIDTH + 1, 1), snake_pos);

	this->screen.draw_pixel(Position(Game::WIDTH + 2, 3), "move snake:" + std::to_string(move_snake_time));

	this->screen.draw_pixel(Position(Game::WIDTH + 2, 4), "draw snake:" + std::to_string(draw_snake_time));

	this->screen.draw_pixel(Position(Game::WIDTH + 2, 5), "draw apple:" +  std::to_string(draw_apple_time));

	this->screen.draw_pixel(Position(Game::WIDTH + 2, 6), "update scoreboard:" + std::to_string(update_scoreboard_time));

	int snake_size = 1;
	Snake snake = this->snake;
	while(snake.has_next()){
		snake = *snake.get_next();
		snake_size++;
	}

	this->screen.draw_pixel(Position(Game::WIDTH + 2, 7), "snake size:" + std::to_string(snake_size));

}

Game::~Game()
{
}

void Game::run()
{
	bool game_over = false;
	while (!game_over)
	{
		try
		{	
			auto frame_start = std::chrono::high_resolution_clock::now();
		
			if (this->keyboard.key_pressed())
			{
				this->controller();
			}

			auto move_snake_start = std::chrono::high_resolution_clock::now();
			this->move_snake();
			auto move_snake_end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> move_snake_time = move_snake_end - move_snake_start;

			auto draw_snake_start = std::chrono::high_resolution_clock::now();
			this->draw_snake(); // snake
			auto draw_snake_end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> draw_snake_time = draw_snake_end - draw_snake_start;

			auto draw_apple_start = std::chrono::high_resolution_clock::now();
			this->draw_apple(); // apple
			auto draw_apple_end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> draw_apple_time = draw_apple_end - draw_apple_start;

			auto update_scoreboard_start = std::chrono::high_resolution_clock::now();
			this->update_scoreboard();
			auto update_scoreboard_end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> update_scoreboard_time = update_scoreboard_end - update_scoreboard_start;

			if(Game::DEBUG)
				this->debug(move_snake_time.count(), draw_snake_time.count(), draw_apple_time.count(), update_scoreboard_time.count());

			auto frame_end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double, std::milli> elapsed = frame_end - frame_start;
			std::chrono::duration<double, std::milli> frame_limit(17);

			std::this_thread::sleep_for(frame_limit - elapsed);
    
		}
		catch (CollisionException &e)
		{
			game_over = true;
			this->draw_snake(); // snake
			while(true){
				if(this->keyboard.key_pressed()){
					if(this->keyboard.get_key_pressed() == Key::ARROW_UP){
						this->score = 0;
						this->erase_snake();
						this->erase_apple();
						this->initialize();
						this->run();

					}
				}
			};
		}
	}
}