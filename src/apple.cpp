#include "apple.h"

void Apple::set_position(const Position &position)
{
	this->position = position;
}

Position Apple::get_position() const
{
	return this->position;
}
	